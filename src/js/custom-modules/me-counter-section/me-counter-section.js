$(window).scroll(function() {
  checkCounterPosition();
});

$(document).ready(function() {
  checkCounterPosition();
});

var checkcounter = 0;

function countTo(){
  $('.me-counter-section__counter-box-text .count-to').each(function() {
    var $this = $(this)
    var countTo = $this.attr('data-count');
    $({ countNum: $this.text()}).animate({
      countNum: countTo
    },
    {
      duration: 4000,
      easing:'swing',
      step: function() {
        $this.text(Math.floor(this.countNum));
      },
      complete: function() {
        $this.text(this.countNum);
      }

    });  
  });
  checkcounter = 1;
}

function checkCounterPosition() {
  var counterposition;
  if($(".me-counter-section__counter-box-text .count-to").length != 0) {
    counterposition = $(".me-counter-section__counter-box-text .count-to").position().top + 100;
  }  
  if (($(window).scrollTop() + $(window).height()) > counterposition && checkcounter == 0) {
    countTo();
  }
}