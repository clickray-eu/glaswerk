
    var geocoder;
    var map;

    function initMap() {

        geocoder = new google.maps.Geocoder();
        
        goToGeocodeAddress('{{ widget.adress_google_maps }}');

        
        var customMapType = new google.maps.StyledMapType([{"featureType":"administrative.province","elementType":"labels.text.fill","stylers":[{"color":"#434242"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#434242"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#434242"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#0073ff"}]}]);
        var customMapTypeId = 'custom_style';
        
        map = new google.maps.Map(document.getElementById('map'), {
            
            zoom: {{ widget.map_zoom }},
            scrollwheel: false,
            disableDefaultUI: true,
            zoomControl: true,
            fullscreenControl: true,
            streetViewControl: true

        });
        
        map.mapTypes.set(customMapTypeId, customMapType);
        map.setMapTypeId(customMapTypeId);
      
    }

    var coustomIcon = '{{ widget.marker_icon.src }}';
    
    function goToGeocodeAddress(address) {
        geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status == 'OK') {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    icon: coustomIcon
                });
            }
        });
    }
