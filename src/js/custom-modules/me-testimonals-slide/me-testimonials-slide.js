// file: Modules/me-testimonal-slide.js
if ($('.me-testimonials-slider').length > 0) {
  $('.me-testimonials-slider .me-testimonials-slide__pagination-element').each(function () {
    $(this).appendTo('.me-testimonials-slider .me-testimonials-nav-slider');
  });

  var slidesNum = $('.me-testimonials-slider .me-testimonials-slide__pagination-element').length;
  if (slidesNum == 1) {
    slidesNum = slidesNum;
  }
  else if (slidesNum <= 5) {
    slidesNum = slidesNum - 1;
  } 
  else {
    slidesNum = 5;
  }

  $('.me-testimonials-slider > span').remove();

  $('.me-testimonials-main-slider > div > span:not(.slick-initialized)').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    arrows: false,
    // adaptiveHeight: true,
    asNavFor: '.me-testimonials-nav-slider',
    focusOnSelect: true,
    initialSlide: findMiddleSlide('.me-testimonials-slide')
  });

  $('.me-testimonials-nav-slider:not(.slick-initialized)').slick({
    infinite: true,
    slidesToShow: slidesNum,
    centerMode: true,
    arrows: false,
    slidesToScroll: 1,
    asNavFor: '.me-testimonials-main-slider > div > span',
    focusOnSelect: true,
    initialSlide: findMiddleSlide('.me-testimonials-slide'),
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 680,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });

  slickLoopTransformFix('.me-testimonials-nav-slider', 'fake-current');
}

function findMiddleSlide(slideClass) {
  var slidesNum, initialSlide;
  slidesNum = $(slideClass).length;
  if (slidesNum > 2) {
    if (slidesNum % 2 == 0) {
      initialSlide = Math.floor(slidesNum / 2) - 1;
    } else {
      initialSlide = Math.floor(slidesNum / 2);
    }
  } else {
    initialSlide = 0;
  }
  return initialSlide;
}

function slickLoopTransformFix(sliderClass, fakeCurrentSlideClass) {
  var slider = $(sliderClass);
  var slidesCount = slider.find('.slick-slide:not(.slick-cloned)').length;
  slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    // when on last slide and next is first
    slider.find('.slick-slide').removeClass(fakeCurrentSlideClass);
    if (currentSlide === slidesCount - 1 && nextSlide === 0) {
      slider.find('.slick-slide[data-slick-index=' + String(slidesCount) + ']').addClass(fakeCurrentSlideClass);
    }
    // when on first slide and next is last
    if (currentSlide === 0 && nextSlide === slidesCount - 1) {
      slider.find('.slick-slide[data-slick-index=-1]').addClass(fakeCurrentSlideClass);
    }
  });
}
// end file: Modules/me-testimonal-slide.js