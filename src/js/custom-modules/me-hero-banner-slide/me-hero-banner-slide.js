if ($('.me-hero-banner-slider .me-hero-banner-slide').length > 0) {
  $('.me-hero-banner-slider .me-hero-banner-nav-slide').each(function() {
    $(this).appendTo('.me-hero-banner-nav-slider');
  });

  $('.me-hero-banner-slider > span').remove();

  $('.me-hero-banner-main-slider > div > span:not(.slick-initialized)').slick({
    autoplay: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplaySpeed: 5000,
    focusOnSelect: true,
    asNavFor: '.me-hero-banner-nav-slider'
  });

  $('.me-hero-banner-nav-slider:not(.slick-initialized)').slick({
    infinite: true,
    slidesToShow: 1,
    centerMode: true,
    centerPadding: "33%",
    arrows: false,
    variableWidth: true,
    slidesToScroll: 1,
    asNavFor: '.me-hero-banner-main-slider > div > span',
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          centerMode: false,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          centerMode: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  setTimeout(function() {
    $('.me-hero-banner-nav-slider .me-hero-banner-nav-slide').addClass('me-hero-banner-nav-slide--visible');
  }, 700);

  slickLoopTransformFix('.me-hero-banner-nav-slider', 'fake-current');
}

function slickLoopTransformFix(sliderClass, fakeCurrentSlideClass) {
  var slider = $(sliderClass);
  var slidesCount = slider.find('.slick-slide:not(.slick-cloned)').length;
  slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    // when on last slide and next is first
    slider.find('.slick-slide').removeClass(fakeCurrentSlideClass);
    if (currentSlide === slidesCount - 1 && nextSlide === 0) {
      slider.find('.slick-slide[data-slick-index=' + String(slidesCount) + ']').addClass(fakeCurrentSlideClass);
    }
    // when on first slide and next is last
    if (currentSlide === 0 && nextSlide === slidesCount - 1) {
      slider.find('.slick-slide[data-slick-index=-1]').addClass(fakeCurrentSlideClass);
    }
  });
}