$(document).ready(function() {

var box_per_page = $('.numer-per-page').text();  
$('.me-website-pagination-wrapper > span > div').each(function() {
    if($(this).index() > parseInt(box_per_page - 1)) { 
    // $(this).hide();
    }
})
});

var current_page = 1;
var max_boxes_on_page = 9;
var filterValue = "";
// var max_page = ;
var item_per_page = $('.numer-per-page').text();
var filterContainer = $('.me-filter-wrapper');

$('.me-filter-wrapper > span > div').each(function(i, item) {
    $(item).attr("data-page", Math.ceil((i + 1) / parseInt(item_per_page - 1 )));
});

function changePage(page) {
    current_page = page;
    filterContainer.isotope({
        filter: function() {
            var name;
            if( filterValue != '*' )
                name = $(this).hasClass(filterValue.replace('.', ''));
            else name = true;

            if( name == true && parseInt($(this).attr("data-page")) == variable.current_page) {
                return true;
            } else {
                return false;
            }
        },
        itemSelector: 'me-website-pagination'
    });

    generatePagination(current_page, max_page);
}

function generatePagination(currentPage, totalPages) {
    $('.me-website-pagination ul').remove();
    var currentPage = currentPage;
    var page = currentPage;
    var total_pages = totalPages;
    var more_pages = total_pages - currentPage;
    var html = $('.me-website-pagination');

    html.append( $( "<ul class='clearfix'></ul>" ) );
    html.find("ul").append("<li><a><i class='fa fa-angle-left'></i></a></li>");

    if (currentPage == 1) {
        html.find("ul li a").attr("href", "javascript:changePage(" + page + ")");
        html.find("ul li").addClass("disabled");
    } else {
        html.find("li a").attr("href", "javascript:changePage(" + (page - 1) + ")");
    }

    if (more_pages == 0) {
        if (currentPage - 4 == 1)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 4) + ")'>" + (page - 4) + "</a></li>");
        if (currentPage - 4 > 1)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 4) + ")'>" + (page - 4) + "</a></li>");
    }

    if (more_pages <= 1) {
        if (currentPage - 3 == 1)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 3) + ")'>" + (page - 3) + "</a></li>");
        if (currentPage - 3 > 1)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 3) + ")'>" + (page - 3) + "</a></li>");
    }

    if (currentPage - 2 == 1)
        html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 2) + ")'>" + (page - 2) + "</a></li>");
    if (currentPage - 2 > 1)
        html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 2) + ")'>" + (page - 2) + "</a></li>");
    if (currentPage - 1 == 1)
        html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 1) + ")'>" + (page - 1) + "</a></li>");
    if (currentPage - 1 > 1)
        html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 1) + ")'>" + (page - 1) + "</a></li>");
        html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page) + ")'>" + (page) + "</a></li>");
        html.find("ul li:last").addClass("active");

    if (currentPage + 1 <= total_pages)
        html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 1) + ")'>" + (page + 1) + "</a></li>");
    if (currentPage + 2 <= total_pages)
        html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 2) + ")'>" + (page + 2) + "</a></li>");

    if (currentPage <= 2) {
        if (currentPage + 3 <= total_pages)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 3) + ")'>" + (page + 3) + "</a></li>");
    }
    if (currentPage <= 1) {
        if (currentPage + 4 <= total_pages)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 4) + ")'>" + (page + 4) + "</a></li>");
    }

    html.find("ul li:last").after("<li><a><i class='fa fa-angle-right'></i></a></li>");

    if (currentPage == total_pages) {
        html.find("li:last a").attr("href", "javascript:changePage(" + (page) + ")");
        html.find("li:last").addClass("disabled");
    } else {
        html.find("li:last a").attr("href", "javascript:changePage(" + (page + 1) + ")");
    }
    if (total_pages > 1 && html.next().is("nav"))
        html.parent().after(html);
}

generatePagination(current_page, max_page);
console.log(current_page);