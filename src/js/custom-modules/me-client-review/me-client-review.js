if ($('.client-reviews-slider').length > 0) {
  $('.client-reviews-slider > span ').slick({
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    infinite: true,
    focusOnSelect: true,
    variableWidth: true,
    centerPadding: "0%",
    initialSlide: findMiddleSlide('.me-client-review__slide'),

    responsive: [
    {
      breakpoint: 1150,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        variableWidth: false,
        centerPadding: "0%",
        centerMode: false
      }
    },
    {
      breakpoint: 760,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: false,
        centerPadding: "0%",
        centerMode: false
      }
    }
    ]
  });

  slickLoopTransformFix('.client-reviews-slider', 'fake-current');
  clientTransformNextPrev();

}



function findMiddleSlide(slideClass) {
  var slidesNum, initialSlide;
  slidesNum = $(slideClass).length;
  if (slidesNum > 2) {
    if (slidesNum % 2 == 0) {
      initialSlide = Math.floor(slidesNum / 2) - 1;
    } else {
      initialSlide = Math.floor(slidesNum / 2);
    }
  } else {
    initialSlide = 0;
  }
  return initialSlide;
}


function slickLoopTransformFix(sliderClass, fakeCurrentSlideClass) {
  var slider = $(sliderClass);
  var slidesCount = slider.find('.slick-slide:not(.slick-cloned)').length;

  slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

    // when on last slide and next is first
    slider.find('.slick-slide').removeClass(fakeCurrentSlideClass);
    if (currentSlide === slidesCount - 1 && nextSlide === 0) {
      slider.find('.slick-slide[data-slick-index=' + String(slidesCount) + ']').addClass(fakeCurrentSlideClass);
    }
    // when on first slide and next is last
    if (currentSlide === 0 && nextSlide === slidesCount - 1) {
      slider.find('.slick-slide[data-slick-index=-1]').addClass(fakeCurrentSlideClass);
    }

  });
}

function clientTransformNextPrev() {
 $('.client-reviews-slider').find('.slick-current').next().addClass('slideNext');
 $('.client-reviews-slider').find('.slick-current').prev().addClass('slidePrev');
 
 var slider = $('.client-reviews-slider > span');
 slider.on('afterChange', function (event, slick, currentSlide) {
  slider.find('.slick-slide').removeClass('slideNext');
  slider.find('.slick-slide').removeClass('slidePrev');
    // var currentSlide = $('.client-reviews-slider .slick-slider').slick('lickCurrentSlide');
    slider.find('.slick-slide[data-slick-index=' + currentSlide + ']').next().addClass('slideNext');
    slider.find('.slick-slide[data-slick-index=' + currentSlide + ']').prev().addClass('slidePrev');

  });
}
