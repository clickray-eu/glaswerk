$(window).load(function() {
    initTilt();
});

$(window).resize(function() {
    initTilt();
});


function initTilt() {
    var deviceWidth = $(window).innerWidth();
    var element = $('.tilt');

    if (deviceWidth < 1200) {
        element.tilt.destroy.call(element);
    } else {
        element.tilt({
            maxTilt: 15,
            perspective: 1000,
            scale: 1, //scale 1 = 100%
            glare: false, // enable glare
            disableAxis: null, // Can be X or Y.
            maxGlare: 1 // From 0 - 1.
        });
    }
}