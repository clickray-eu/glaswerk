function timelineProgress() {
  var check = 0;
  $('.me-timeline-item-wrapper > span').append('<div class="progress-bar"></div>');

  var progressBar = $('.progress-bar');

  var timeLineOffsetTop = $('.me-timeline-item-wrapper > span').offset().top;
  var timelineheight = $('.me-timeline-item-wrapper > span').innerHeight();

  var timeLineEnd = timeLineOffsetTop + timelineheight;

  var timeAnimation = timelineheight * 4;

  var endText = $('.timeline-end-text span').html();
  var endTextHeight = $('.timeline-end-text span').innerHeight();
  $(window).scroll(function () {
    var scroll = $(window).scrollTop();

    if (scroll > timeLineOffsetTop - 800 && scroll < timeLineEnd) {
      progressBar.animate({
        height: timelineheight - 140 + "px"
      }, timeAnimation);

      $('.me-timeline-item-wrapper').addClass('animate');
    }

    if ($('.me-timeline-item-wrapper.animate').length != 0 && check == 0) {
      check = 1;
      setTimeout(function () {
        $('.me-timeline-item-wrapper>span').append('<div style="position: absolute; top: ' + (timelineheight - 148) + 'px"><div class="dot-end wow fadeIn"></div></div>');
        $('.me-timeline-item-wrapper>span').append('<div style="position: absolute; top: ' + (timelineheight - endTextHeight/2 - 140) + 'px"><div class="text-end wow fadeInUp">' + endText + '</div></div>');
      }, timeAnimation);
    }
  });
}
timelineProgress();