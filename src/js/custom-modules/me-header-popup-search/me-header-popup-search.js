if ($('.me-header-popup-search').length) {
    manageHeaderSearch();
}

function manageHeaderSearch() {
    $('.me-header-popup-search #search-btn').on('click', (e) => {
        $(e.target).closest('.input-wrapper').find('.search-popup').fadeIn(500);
        $('body').css('overflow', 'hidden');
    })
    $('.me-header-popup-search .search-popup').on('click', (e) => {
        $(e.target).closest('.input-wrapper').find('.search-popup').fadeOut(500);
        e.stopPropagation();
        $('body').css('overflow', '');
    })
    $('.me-header-popup-search .popup-inner').on('click', (e) => {
        e.stopPropagation();
    })
}