$(document).ready(function () {
	if ($('.me-accordion-panel').length > 0) {
		var img_src = $('img.inner-img').attr('src');
		$('img.inner-img').parent().css('background-image', 'url("' + img_src + '")')

	}
	accordionPanles();


	if ($('.me-accordion-wrapper').length > 0) {
		setTimeout(function () {
			$(".me-accordion-wrapper span > div").first().find('me-accordion-panel__header a').click();
		}, 10)
	}

})

function accordionPanles() {

	$('.me-accordion-panel__header').click(function(e) {
		e.preventDefault();

		var $this = $(this).parent();

		if ($this.hasClass('open')) {

			$this.removeClass('open');
			$this.children('.me-accordion-panel__body').slideUp(350);


		} else {

			$this.parent().parent().find('.me-accordion-panel').removeClass('open');
			$this.parent().parent().find('.me-accordion-panel__body').slideUp(350);
			$this.toggleClass('open');
			$this.children('.me-accordion-panel__body').slideToggle(350);

		}
	});
}