if ($('.me-testimonials-slider-v2').length > 0) {

  $('.me-testimonials-slider-v2 > span:not(.slick-initialized)').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    adaptiveHeight: true,
    focusOnSelect: true
  });
}