if($('.me-tab-panels').length > 0) {
  var count=0;
  $('.me-tab-panels__main .me-tab-panel').each(function() {
    if ($(this).attr('data-icon')) {
      var icon = "<i class='" + $(this).data('icon') + "'></i>";
    } else {
      var icon = '';
    }
    $(this).parent().attr('id', 'tab-' + count);
    if(count==0){
      $(".me-tab-panels__nav").append("<li class='active'><a href=#tab-" + count + ">" + icon + $(this).attr("name") + "</a></li>");
    }
    else{
      $(".me-tab-panels__nav").append("<li><a href=#tab-" + count + ">" + icon + $(this).attr("name") + "</a></li>");
    }
    count++;
  });
}

$('.me-tab-panels__nav a').on('click', function(e) {
  e.preventDefault();
  var tabId = $(this).attr('href');
  $('.me-tab-panels__nav li').removeClass('active');
  $(this).parent().addClass('active');
  $('.me-tab-panels__main .hs_cos_wrapper_type_custom_widget').fadeOut().promise().done(function() {
    $(tabId).fadeIn();
  });
  if ($(window).width() < 767) {
    var tabOffset = $(this).parents('.me-tab-panels').find('.me-tab-panels__main').offset().top - 100;
    $('html, body').animate({
      scrollTop: tabOffset + 'px'
    }, '2000');
  }
});