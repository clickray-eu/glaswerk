function cloneCtaWrapper(){
    if ($('.header-main-content .cta-header').length) {
        const ctawrapperEl = $("<li class='hs-menu-item hs-menu-depth-1 cta-element'></li>");
        ctawrapperEl.appendTo('.slicknav_nav .hs-menu-wrapper > ul')
        $('.header-main-content .cta-header').clone(true).appendTo('.slicknav_nav .hs-menu-wrapper > ul > li:last-of-type');
    }
}

waitForLoad(".header-main-content", ".cta-header", cloneCtaWrapper);

(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).each(function(i,container){
            if($(container).find(">"+targetSelector).length==0){
                $(container).find(targetSelector).appendTo($(container));
                $(container).find('.hs_cos_wrapper_type_custom_widget').remove();
                $(container).find('.hs_cos_wrapper_type_widget_container').remove();
                $(container).find('.widget-span').remove();
            }
        });
    };
})(jQuery);