$(document).ready(function(){
	globalVideoAutoplay();
});

// GLOABL AUTO PLAY 
function globalVideoAutoplay(){

	if ($('body').hasClass('ios')) {
		iosLoadVideo();
	} else {
		$('video').each(function(){
			var attr = $(this).attr('autoplay');

			if (typeof attr !== typeof undefined && attr !== false) {
				$(this)[0].play();
			}
		});
	}
}

function iosLoadVideo(){

	$('video').each(function(){
		$(this)[0].load();
	});
}