if ($('.client-reviews-slider').length > 0) {
  $('.client-reviews-slider > span ').slick({
    centerMode: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    dots: true,

    infinite: true,
    draggable: false,
    swipe: false,
    focusOnSelect: true,
    variableWidth: true,
    centerPadding: "33%",
    initialSlide: findMiddleSlide('.me-client-review__slide'),

    responsive: [
      {
        breakpoint: 1150,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          variableWidth: false,
          centerPadding: "0%",
          centerMode: false
        }
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          variableWidth: false,
          centerPadding: "0%",
          centerMode: false
        }
      }
    ]
  });
}

$( ".slick-center" ).next().addClass('slidePrev');
$( ".slick-center" ).prev().addClass('slideNext');
jQuery('.client-reviews-slider > span').on('afterChange beforeChange', function(event, slick, currentSlide){
  $( ".slick-slide").each(function(){
    $( ".slick-slide").removeClass('slidePrev');
    $( ".slick-slide" ).removeClass('slideNext');
  });
  $( ".slick-center" ).next().toggleClass('slidePrev');
  $( ".slick-center" ).prev().toggleClass('slideNext');
});


function findMiddleSlide(slideClass) {
  var slidesNum, initialSlide;
  slidesNum = $(slideClass).length;
  if (slidesNum > 2) {
    if (slidesNum % 2 == 0) {
      initialSlide = Math.floor(slidesNum / 2) - 1;
    } else {
      initialSlide = Math.floor(slidesNum / 2);
    }
  } else {
    initialSlide = 0;
  }
  return initialSlide;
}


