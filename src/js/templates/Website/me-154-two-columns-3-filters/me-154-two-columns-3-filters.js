
var item_per_page=6;
(function($) {
$.fn.cutWrappers = function(targetSelector) {
$(this).find(targetSelector).appendTo($(this));
$(this).find('.hs_cos_wrapper_type_custom_widget').remove();
$(this).find('.hs_cos_wrapper_type_widget_container').remove();
$(this).find('.widget-span').remove();
}
}(jQuery)); 
$('.listing-blog').cutWrappers('.case-studies');
var current_page = 1;
var filterValue = "";
var max_page = 1;
///
var resourcesContainer = $('.listing-blog');
$('.sidebar-submenu div.hs-menu-wrapper>ul>li:first a').addClass('active');
$(".listing-blog  > section").each(function(i, item) {
$(item).attr("data-page", Math.ceil((i + 1) / item_per_page));
});
///
function scrollTop() {
$("html, body").animate({
scrollTop: $("div.listing-blog").position().top - 60
}, 1000);
}
filterGrid();
function filterGrid() {
$('.sidebar-submenu li a').on('click', function() {
current_page = 1;
var category_items_length = $(".case-studies" + $(this).data('filter')).length;
max_page = Math.ceil(category_items_length / item_per_page);
var category_items = $(".case-studies" + $(this).data('filter'));
category_items.removeAttr("data-page");
category_items.each(function(i, item) {
$(item).attr("data-page", Math.ceil((i + 1) / item_per_page));
});
generatePagination(current_page, max_page);
$('.sidebar-submenu li a').removeClass('active');
$(this).addClass('active');
filterValue = $(this).attr('data-filter');
resourcesContainer.isotope({
filter: function() {
var name;
if (filterValue != '*')
name = $(this).hasClass(filterValue.replace('.', ''));
else name = true;
if (name == true && parseInt($(this).attr("data-page")) == current_page) {
return true;
} else {
return false;
}
},
itemSelector: '.case-studies',
layoutMode: 'fitRows'
});
})
}
// add data filter for isotope 
(function(){
var menuItem = $('.sidebar-submenu li > a');
menuItem.each(function(i,element){
if(i==0){
$(element).attr('data-filter', "*");             
}else
$(element).attr('data-filter', "." + $(element).text().toLowerCase().replace(" ", "-"));
});
})();
        
// submenu configuration 

$( ".sidebar-submenu li > a" ).on('click', function(e) {
    e.preventDefault();
    
        var element = $(this).parent('li');
if(element.hasClass( "show-submenu" )){
                    if (element.hasClass("hs-item-has-children")) {
                        element.removeClass('show-submenu');
                    }
                    element.find('li').removeClass('show-submenu');
                    element.find('ul').slideUp();

   
    }
    else{
          if (element.hasClass("hs-item-has-children")) {
                        element.addClass('show-submenu');
                    }
                /*  element.children('ul').slideDown(); */
                     element.siblings('li').children('ul').slideUp();
                   element.siblings('li').removeClass('show-submenu');

        }       
        });
    
         // active for menu 
        
        
        $( "li.hs-menu-depth-1 > a" ).click(function() {
            $(".sidebar-submenu a").removeClass("active-child");
            $(this).addClass('active-child');        
        });
        
        
        // active for submenu 
        $( ".hs-menu-children-wrapper li > a" ).click(function() {
            $(".sidebar-submenu a").removeClass("active");
            $(this).addClass('active');     
        });

     (function($) {
        $.fn.cutWrappers = function(targetSelector) {
            $(this).find(targetSelector).appendTo($(this));
            $(this).find('.hs_cos_wrapper_type_custom_widget').remove();
            $(this).find('.hs_cos_wrapper_type_widget_container').remove();
            $(this).find('.widget-span').remove();
        }
    }(jQuery));

    $('.listing-blog').cutWrappers('.case-studies');


    var current_page = 1;
    var filterValue = "";
    var max_page = 1;

    
    ///
    var resourcesContainer = $('.listing-blog');
    $('.sidebar-submenu div.hs-menu-wrapper>ul>li:first a').addClass('active');
    
    $(".listing-blog  > section").each(function(i, item) {
        $(item).attr("data-page", Math.ceil((i + 1) / item_per_page));
     });
    
    ///
    
    function scrollTop() {
        $("html, body").animate({
            scrollTop: $("div.listing-blog").position().top - 60
        }, 1000);
     }
    
    ///
    //   
    ///
    filterGrid();


    function filterGrid() {

        $('.sidebar-submenu li a').on('click', function() {
            current_page = 1;
            var category_items_length = $(".case-studies" + $(this).data('filter')).length;
            max_page = Math.ceil(category_items_length / item_per_page);
            var category_items = $(".case-studies" + $(this).data('filter'));
            category_items.removeAttr("data-page");
            category_items.each(function(i, item) {
                $(item).attr("data-page", Math.ceil((i + 1) / item_per_page));
            });
            generatePagination(current_page, max_page);
            $('.sidebar-submenu li a').removeClass('active');
            $(this).addClass('active');
            filterValue = $(this).attr('data-filter');



            resourcesContainer.isotope({
                filter: function() {
                    var name;
                    if (filterValue != '*')
                        name = $(this).hasClass(filterValue.replace('.', ''));
                    else name = true;
                    if (name == true && parseInt($(this).attr("data-page")) == current_page) {
                        return true;
                    } else {
                        return false;
                    }
                },
                itemSelector: '.case-studies',
                layoutMode: 'fitRows'
            });
        })
    }


    // first load 
    setTimeout(function() {
       $('.sidebar-submenu  li a.active').click();
     }, 10);


    function changePage(page) {
        current_page = page;
        resourcesContainer.isotope({
            filter: function() {
                var name;
                if (filterValue != '*')
                    name = $(this).hasClass(filterValue.replace('.', ''));
                else name = true;
                if (name == true && parseInt($(this).attr("data-page")) == current_page) {
                    return true;
                } else {
                    return false;
                }
            },
            itemSelector: '.case-studies',
            layoutMode: 'fitRows'
        });
        
        scrollTop();
        generatePagination(current_page, max_page);
    }

    function generatePagination(currentPage, totalPages) {
        $("nav.case-study-pagination").remove();
        var currentPage = currentPage;
        var page = currentPage;
        var total_pages = totalPages;
        var more_pages = total_pages - currentPage;
        var html = "";
        html = $("<nav class='case-study-pagination'><ul class='pagination clearfix'></ul></nav>");
        html.find("ul").append("<li><a><i class='fa fa-angle-left'></i><i class='fa fa-angle-left'></i></a></li>");
        if (currentPage == 1) {
            html.find("li a").attr("href", "javascript:changePage(" + page + ")");
            html.find("li").addClass("disabled");
        } else {
            html.find("li a").attr("href", "javascript:changePage(" + (page - 1) + ")");
        }

        if (more_pages == 0) {
            if (currentPage - 4 == 1)
                html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 4) + ")'>" + (page - 4) + "</a></li>");
            if (currentPage - 4 > 1)
                html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 4) + ")'>" + (page - 4) + "</a></li>");

        }
        if (more_pages <= 1) {
            if (currentPage - 3 == 1)
                html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 3) + ")'>" + (page - 3) + "</a></li>");
            if (currentPage - 3 > 1)
                html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 3) + ")'>" + (page - 3) + "</a></li>");

        }
        if (currentPage - 2 == 1)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 2) + ")'>" + (page - 2) + "</a></li>");
        if (currentPage - 2 > 1)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 2) + ")'>" + (page - 2) + "</a></li>");
        if (currentPage - 1 == 1)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 1) + ")'>" + (page - 1) + "</a></li>");
        if (currentPage - 1 > 1)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page - 1) + ")'>" + (page - 1) + "</a></li>");

        html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page) + ")'>" + (page) + "</a></li>");
        html.find("ul li:last").addClass("active");

        if (currentPage + 1 <= total_pages)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 1) + ")'>" + (page + 1) + "</a></li>");
        if (currentPage + 2 <= total_pages)
            html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 2) + ")'>" + (page + 2) + "</a></li>");

        if (currentPage <= 2) {
            if (currentPage + 3 <= total_pages)
                html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 3) + ")'>" + (page + 3) + "</a></li>");
        }
        if (currentPage <= 1) {
            if (currentPage + 4 <= total_pages)
                html.find("ul li:last").after("<li><a href='javascript:changePage(" + (page + 4) + ")'>" + (page + 4) + "</a></li>");
        }

        html.find("ul li:last").after("<li><a><i class='fa fa-angle-right'></i><i class='fa fa-angle-right'></i></a></li>");

        if (currentPage == total_pages) {
            html.find("li:last a").attr("href", "javascript:changePage(" + (page) + ")");
            html.find("li:last").addClass("disabled");
        } else {
            html.find("li:last a").attr("href", "javascript:changePage(" + (page + 1) + ")");
        }
        if (total_pages > 1 && !$(".listing-blog").parent().next().is("nav"))
            $(".listing-blog").parent().after(html);
    }


    filterGrid();

