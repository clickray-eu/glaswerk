'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    htmlmin = require('gulp-htmlmin'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync'),
    sourcemaps = require('gulp-sourcemaps'),
    clean = require('gulp-clean'),
    include = require('gulp-include'),
    fileinclude = require('gulp-file-include'),
    gutil = require('gulp-util'),
    path = require('path'),
    watch = require('gulp-watch'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat-util'),
    download = require('gulp-download'),
    runSequence = require('run-sequence'),
    replace = require('gulp-replace'),
    minify = require('gulp-minifier'),
    minifyCss = require("gulp-minify-css"),
    sassGlob = require('gulp-sass-glob'),
    addsrc = require('gulp-add-src'),
    change = require('gulp-change'),
    changed = require('gulp-changed'),
    modifyFile = require('gulp-modify-file'),
    sourceChanged = require('gulp-changed-in-place'),
    clean = require('gulp-clean'),
    notifier = require('node-notifier'),
    autoprefixer = require('gulp-autoprefixer');
    
var currentTask="";
var user = process.argv[3];
if (user == undefined) {
    user = "global";
}

var dev_version = process.argv[5];
if (dev_version == true || dev_version == undefined) {
    dev_version = true;
} else if (dev_version == false) {
    dev_version = false;
}
changeDeveloperVersion();


/* DECLARATIONS */

var modulesCss = ['src/scss/custom-modules/**/*.scss'];
var modulesJs = ['src/js/custom-modules/**/*.js'];
var templatesCss = ['src/scss/templates/**/*.scss'];
var templatesJs = ['src/js/templates/**/*.js'];

var base = ['src/scss/base/**/*.scss'];
var baseJs = ['src/js/base/**/*.js'];
var componentsJs = ['src/js/components/**/*.js'];
var extendsScss = ['src/scss/extends/**/*.scss'];
var layout = ['src/scss/layout/**/*.scss'];

var vendorsCss = ['src/scss/vendors/**/*.scss'];
var vendorsJs = ['src/js/vendors/**/*.js'];

var module_config = ['config.json'];

/* ******************************************************************************************************************************************************************************************************************************************************************** */

/* FUNCTIONS */

function changeDeveloperVersion() {
    let fs = require('fs');
    fs.readFile('./src/scss/variables/variables.scss', (err, data) => {
        data = data.toString();
        let searchVersion = new RegExp("[$]dev_version[ ]*[:][ ]*([a-z]+)[;]", "g");
        data = data.replace(searchVersion, "$dev_version : " + dev_version + ";")
        fs.writeFile('./src/scss/variables/variables.scss', data, function () {})
    })
}

var walk = (dir, done) => {
    let fs = require('fs');
    let path = require('path');
    let results = [];
    fs.readdir(dir, (err, list) => {
        if (err) return done(err);
        let pending = list.length;
        if (!pending) return done(null, results);
        list.forEach((file) => {
            file = path.resolve(dir, file);
            fs.stat(file, (err, stat) => {
                if (stat && stat.isDirectory()) {
                    walk(file, (err, res) => {
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else {
                    let document = file.split("/build/" + user + "/");
                    if (document.length == 1)
                        results.push(file);
                    else results.push(document[1]);
                    if (!--pending) done(null, results);
                }
            });
        });
    });
};

function useLocalStyle(results) {
    var fs = require('fs');

    fs.readFile('./build/' + user + '/index2.html', (err, data) => {
        var replace;
        data = data.toString();
        results.forEach(function (file) {
            let fileArray= file.split('/');
            if (file.search('.css') > -1) {
                replace = new RegExp("href[=][\"\'](.*?)(" + fileArray[fileArray.length-1].replace("/", "\/").replace(".css","[.]") + ")[^\"\']*[\"\']", "g");
                data = data.replace(replace, "href='" + file + "'");
            } else {
                replace = new RegExp("src[=][\"\'](.*?)(" + fileArray[fileArray.length-1].replace("/", "\/").replace(".js","[.]") + ")[^\"\']*[\"\']", "g");
                data = data.replace(replace, "src='" + file + "'");
            }
        })
        fs.writeFile('./build/' + user + '/index.html', data, function () {});
    })
}

var autoInclude = (content) => {
    content = "@import '" + __dirname + "/src/scss/variables/variables.scss'; " + content;
    content = "@import '" + __dirname + "/src/scss/extends/extends.scss'; " + content;
    content = "@import '" + __dirname + "/src/scss/extends/mixins.scss'; " + content;

//    content = "@import '" + __dirname + "/src/scss/base/variables.scss'; " + content;
    return content;
}
var addFileName = (content,path,file) => {
    var filePath="/*[****** "+path.replace(__dirname+"/","")+" ******]*/";
    
    return `${filePath}${content}`;
}
var extendInclude = (content) => {
    content = "@import '" + __dirname + "/src/scss/extends/extends.scss'; " + content;
    return content;
}

var helpersInclude = (content) => {
    content = "@import '" + __dirname + "/src/scss/helpers/breakpoint.scss'; " + content;
    content = "@import '" + __dirname + "/src/scss/helpers/mixin.scss'; " + content;
    return content;
}

var baseInclude = (content) => {
    content = "@import '" + __dirname + "/src/scss/include/base.scss'; " + content;
    return content;
}

var layoutInclude = (content) => {
    content = "@import '" + __dirname + "/src/scss/include/layouts.scss'; " + content;
    return content;
}

/* ******************************************************************************************************************************************************************************************************************************************************************** */

/* TASKS */

gulp.task('clean', () => {
    currentTask="clean";
    return del(['./build/' + user])
})

gulp.task('reload', () => {
    currentTask="reload";
    browserSync.reload();
})

gulp.task('serve',['rebuild'], () => {
    currentTask="serve";
    browserSync.init({
        server: {
            baseDir: 'build/' + user + '/',
            files: ['./build/' + user + '/**/*.css', './build/' + user + '/**/*.js']
        },
        open: false
    })
})

gulp.task('module_config', () => {
    currentTask="module_config";
    let fs = require('fs');
    let downloadHtml = (config_json) => {
        download(config_json.download_url)
            .pipe(rename("index2.html"))
            .pipe(replace("/cdn-cgi/", "http://www.cloudflare.com/cdn-cgi/"))
            .pipe(replace("/hs/hsstatic/", "https://preview.hs-sites.com/hs/hsstatic/"))
            .pipe(replace("/_hcms/preview/", "https://preview.hs-sites.com/_hcms/preview/"))
            .pipe(replace("/_hcms/forms/", "https://preview.hs-sites.com/_hcms/forms/"))
            .pipe(gulp.dest('build/' + user + '/'));
    }
    fs.readFile('./config.json', (err, data) => {
        if (err == null) {
            let config_json = JSON.parse(data.toString());
            let replace_js = new RegExp("src[=][\"\'](.*?)(" + config_json.js_replace + ")[^\"\']*[\"\']", "g");
            let replace_css = new RegExp("href[=][\"\'](.*?)(" + config_json.css_replace + ")[^\"\']*[\"\']", "g");
            downloadHtml(config_json);

        } else {
            let config_json = {
                'download_url': 'http://example.com'
            };
            fs.writeFile('./config.json', JSON.stringify(config_json), (err) => {
                downloadHtml(config_json);
            })
        }
    });
})

gulp.task('recursiveDirectoryFiles', () => {
    currentTask="recursiveDirectoryFiles";
    let fs = require('fs');
    let path = require('path');
    setTimeout(function(){
        walk('./build/' + user, (err, results) => {
            if (err) throw err;
            useLocalStyle(results);
            console.log("SOURCE FILE REPLACED");
        });    
    },1000);
})

gulp.task('base', () => {
    currentTask="base";
    var versionDate = new Date();
    gulp.src("./src/scss/base/template.scss")
        .pipe(sourceChanged({firstPass:true}))
        .pipe(browserSync.stream())
        .pipe(sassGlob())
        .on('error', swallowError)
        .pipe(change(autoInclude))
        .pipe(change(extendInclude))
        .pipe(rename('base.scss'))
        .pipe(gulp.dest('./src/scss/include/'))
        .pipe(sass())
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['>0%']
        }))
        .on('error', swallowError)
        .pipe(replace("/*==", ""))
        .pipe(replace("==*/", ""))
        .pipe(replace("/*=!=", ""))
        .pipe(replace("=!=*/", ""))

        .pipe(concat('base.css'))
        .pipe(changed('build/' + user + '/'))
        .pipe(gulp.dest('build/' + user + '/'))
        .pipe(browserSync.stream())
})

gulp.task('layout', () => {
    currentTask="layout";
    var versionDate = new Date();
    gulp.src(layout)
        .pipe(sourceChanged({firstPass:true}))
        .pipe(browserSync.stream())
        .pipe(change(autoInclude))
        .pipe(sassGlob())
        .on('error', swallowError)
        .pipe(change(autoInclude))
        .pipe(change(extendInclude))
        .pipe(rename('layouts.scss'))
        .pipe(gulp.dest('./src/scss/include/'))
        .pipe(sass())
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['>0%']
        }))

        .pipe(replace("/*==", ""))
        .pipe(replace("==*/", ""))
        .pipe(replace(/\/[*]=!=[^\n]*[\n]/gm, ""))

        .pipe(concat('layout.css'))
        .pipe(changed('build/' + user + '/'))
        .pipe(gulp.dest('build/' + user + '/'))
        .pipe(browserSync.stream())
})

gulp.task('modulesCss', () => {
    currentTask="modulesCss";
    var versionDate = new Date();
    gulp.src(modulesCss)
        .pipe(sourceChanged({firstPass:true}))
        .pipe(sassGlob())
        .on('error', swallowError)
        .pipe(change(autoInclude))
        .pipe(modifyFile(addFileName))
        .pipe(sass())
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['>0%']
        }))
        .on('error', swallowError)
        .pipe(replace("/*==", ""))
        .pipe(replace("==*/", ""))
        .pipe(replace(/\/[*]=!=[^\n]*[\n]/gm, ""))
        .pipe(replace(/\/[*][\[]del[\]][^\n]*[\n]/gm, ""))
        
        .pipe(changed('build/' + user + '/modules/'))
        .pipe(gulp.dest('build/' + user + '/modules/'))
        .pipe(browserSync.stream())
        .pipe(rename((path) => {
            path.extname = ".min" + path.extname;
        }))
        .pipe(minifyCss())
        .on('error', swallowError)
        .pipe(gulp.dest('build/' + user + '/modules/'))
        .pipe(browserSync.stream());
});

gulp.task('modulesJs', () => {
    currentTask="modulesJs";
    var versionDate = new Date();
    gulp.src(modulesJs)
        .pipe(sourceChanged({firstPass:true}))
        .pipe(changed('build/' + user + '/modules/'))
        .pipe(gulp.dest('build/' + user + '/modules/'))
})

gulp.task('templatesCss', () => {
    currentTask="templatesCss";
    var versionDate = new Date();
    gulp.src(templatesCss)
        .pipe(sourceChanged({firstPass:true}))
        .pipe(sassGlob())
        .on('error', swallowError)
        .pipe(change(autoInclude))
        .pipe(change(baseInclude))
        .pipe(change(layoutInclude))
        .pipe(sass())
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['>0%']
        }))
        .on('error', swallowError)
        .pipe(replace("/*==", ""))
        .pipe(replace("==*/", ""))
        .pipe(changed('build/' + user + '/templates/'))

        .pipe(gulp.dest('build/' + user + '/templates/'))
        .pipe(browserSync.stream())
        .pipe(rename((path) => {
            path.extname = ".min" + path.extname;
        }))
        .pipe(minifyCss())
        .on('error', swallowError)
        .pipe(gulp.dest('build/' + user + '/templates/'))
        .pipe(browserSync.stream());
});

gulp.task('templatesJs', () => {
    currentTask="templatesJs";
    var versionDate = new Date();
    gulp.src(templatesJs)
        .pipe(sourceChanged({firstPass:true}))
        .pipe(changed('build/' + user + '/templates/'))
        .pipe(gulp.dest('build/' + user + '/templates/'))
})

gulp.task('baseJs', () => {
    currentTask="baseJs";
    var versionDate = new Date();
    gulp.src(baseJs)
        .pipe(sourceChanged({firstPass:true}))
        .pipe(concat.header('// file: <%= file.relative %>\n'))
        .pipe(concat.footer('\n// end file: <%= file.relative %>\n'))
        .pipe(concat('main.js'))
        .pipe(changed('build/' + user + '/base/'))
        .pipe(gulp.dest('build/' + user + '/base/'))
})
gulp.task('componentsJs', () => {
    currentTask="componentsJs";
    var versionDate = new Date();
    gulp.src(componentsJs)
        .pipe(sourceChanged({firstPass:true}))
        .pipe(changed('build/' + user + '/components/'))
        .pipe(gulp.dest('build/' + user + '/components/'))
})


gulp.task('vendorsCss', () => {
    currentTask="vendorsCss";
    var versionDate = new Date();
    gulp.src(vendorsCss)
        .pipe(sourceChanged({firstPass:true}))
        .pipe(sass())
        .on('error', swallowError)
        .pipe(replace("/*=!=", ""))
        .pipe(replace("=!=*/", ""))
        .pipe(changed('build/' + user + '/vendors/'))
        .pipe(gulp.dest('build/' + user + '/vendors/'))
});

gulp.task('vendorsJs', () => {
    currentTask="vendorsJs";
    var versionDate = new Date();
    gulp.src(vendorsJs)
        .pipe(sourceChanged({firstPass:true}))
        .pipe(sass())
        .on('error', swallowError)
        .pipe(replace("/*=!=", ""))
        .pipe(replace("=!=*/", ""))
        .pipe(changed('build/' + user + '/vendors/'))
        .pipe(gulp.dest('build/' + user + '/vendors/'))
});
gulp.task('clean-scripts', function () {
    currentTask="clean-scripts";
    return gulp.src('build/' + user,{read:false})
    .pipe(clean());
  });
gulp.task('rebuild',['clean-scripts'],function(){
    currentTask="clean-scripts";
    gulp.src('build/'+user,{read:false})
    .pipe(sourceChanged({clear:true}));
    runSequence(['base','baseJs', 'layout', 'modulesCss', 'modulesJs','componentsJs', 'templatesCss', 'templatesJs', 'vendorsCss', 'vendorsJs','module_config']);
});
gulp.task('default', ['serve'], () => {
    watch(base, () => {
        gulp.start('clean-scripts');
        gulp.start('rebuild');
    });
    watch(extendsScss, () => {
        gulp.start('clean-scripts');
        gulp.start('rebuild');
    })
    watch(layout, () => {
        gulp.start('clean-scripts');
        gulp.start('rebuild');
    });
    watch(modulesCss, () => {
        gulp.start('modulesCss');
    });
    watch(modulesJs, () => {
        gulp.start('modulesJs');
    }).on('change', browserSync.reload);

    watch(templatesCss, () => {
        gulp.start('templatesCss');
    });

    watch(templatesJs, () => {
        gulp.start('templateJs');
    }).on('change', browserSync.reload);

    watch(baseJs, () => {
        gulp.start('baseJs');
    }).on('change', browserSync.reload);
    watch(componentsJs, () => {
        gulp.start('componentsJs');
    }).on('change', browserSync.reload);

    watch(vendorsCss, () => {
        gulp.start('vendorsCss');
    });
    watch(vendorsJs, () => {
        gulp.start('vendorsJs');
    });
    watch(module_config, () => {
        gulp.start('module_config');
    });
    watch("./build/" + user + "/index2.html", () => {
        gulp.start('recursiveDirectoryFiles');
        browserSync.reload();
    });
});

/* ******************************************************************************************************************************************************************************************************************************************************************** */

var swallowError = function (error) {
 
    var lineNumber = error.line;
    var pluginName = "["+currentTask+"]";
    gulp.src('build/'+user,{read:false})
    .pipe(sourceChanged({clear:true}));
    if(error.file.search('.scss')>-1){
        notifier.notify({
            title: 'Failed: ' +pluginName+ " ["+error.relativePath+"] on line "+lineNumber,
            message: error.messageOriginal,
            icon: path.join(__dirname, 'sass.png'), 
            sound:true,
            wait:true,
            timeout:10
        });
    }else if(error.file.search('.js')>-1){
        notifier.notify({
            title: 'Failed: ' +pluginName + "[js/template.js]",
            message: error.messageOriginal,
             sound:true,
             icon: path.join(__dirname, 'js.png'),
             wait:true,
             timeout:10
            }).write(error);        
    }
 
    gutil.beep();
 
    var report = '';
    var chalk = gutil.colors.white.bgRed;
 
    report += chalk('TASK:') + pluginName+'\n';
    report += chalk('ERROR:') + ' ' + error.message + '\n';
    if (error.line) { report += chalk('LINE:') + ' ' + error.line + '\n'; }
    if (error.fileName) { report += chalk('FILE:') + ' ' + error.fileName + '\n'; }
 
    console.error(report);
 
    this.emit('end');
}